import PropTypes from "prop-types";
import BasketCard from "./BasketCard.jsx";
import Modal from "../../Components/Modal.jsx";
import ModalWrapper from "../../Components/ModalWrapper.jsx";
import ModalFooter from "../../Components/ModalFooter.jsx";
import Button from "../../Components/Button.jsx";

const Basket = ({
                    isOpenFirst,
                    actionFirst,
                    removeShop,
                    handleCurrentCard,
                    currentCard,
                    shop,
                    buyBtn,
                    buyClick,
                }) => {

        return (
            <>
                {isOpenFirst && (
                    <ModalWrapper click={actionFirst}>
                        <Modal actionFirst={actionFirst}
                        ><h2 className='basket-question'>Видалити товар?</h2>
                            <ModalFooter>
                                <Button click={actionFirst} classNames="modal-btn">СКАСУВАТИ</Button>
                                <Button click={() => {
                                    actionFirst()
                                    removeShop(currentCard.article)
                                    buyClick(currentCard.article)
                                }}
                                        classNames="modal-btn-active"
                                >
                                    ВИДАЛИТИ</Button>
                            </ModalFooter>
                        </Modal>
                    </ModalWrapper>
                )}
                <div className='shop'>
                    {
                        shop.map(({article, name, img, price, color}) => {
                            return (
                                <BasketCard
                                    actionFirst={actionFirst}
                                    handleCurrentCard={handleCurrentCard}
                                    buyBtn={buyBtn}
                                    key={article}
                                    alt={name}
                                    img={img}
                                    name={name}
                                    article={article}
                                    price={price}
                                    color={color}
                                >
                                </BasketCard>
                            )
                        })
                    }
                </div>
            </>
        );
    };

Basket.propTypes = {
    shop: PropTypes.array,
    isOpenFirst: PropTypes.bool,
    actionFirst: PropTypes.func,
    removeShop: PropTypes.func,
    handleCurrentCard: PropTypes.func,
    currentCard: PropTypes.object,
    buyBtn: PropTypes.any,
    buyClick: PropTypes.func,
}

export default Basket;