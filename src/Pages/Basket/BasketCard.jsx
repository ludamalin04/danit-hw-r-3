import PropTypes from "prop-types";
import Close from "../../Components/Close.jsx";

const BasketCard = ({
                        color,
                        article,
                        price,
                        name,
                        img,
                        alt,
                        actionFirst,
                        handleCurrentCard,
                    }) => {
    const basketItem = {color, article, price, name, img, alt}
    return (
        <div className='shop-card'>
            <Close click={() => {
                actionFirst()
                handleCurrentCard(basketItem)
            }}/>
            <div>
                <img src={img} alt={alt}/>
            </div>
            <div className='shop-card-content'>
                <h3 className='shop-card-name'>{name}</h3>
                <span className='shop-card-article'>артикул {article}</span>
                <h3 className='shop-card-color'>{color}</h3>
                <div className='shop-card-buy'>
                    <h3><span className='shop-card-price'>{price}</span>грн</h3>
                </div>
            </div>
        </div>
    );
};

BasketCard.propTypes = {
    color: PropTypes.string,
    handleCurrentCard: PropTypes.func,
    actionFirst: PropTypes.func,
    article: PropTypes.number,
    price: PropTypes.number,
    name: PropTypes.string,
    img: PropTypes.string,
    alt: PropTypes.string,
}

export default BasketCard;