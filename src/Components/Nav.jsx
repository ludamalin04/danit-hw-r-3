import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import Heart from "./Heart.jsx";
import './Components.scss'
import Cart from "./Cart.jsx";

const Nav = ({favorite, shop}) => {
    return (
        <div className='header-top'>
            <div>
                <Link className='logo' to={'/'}><img src='../../public/imgs/Logo-Blueband.png' alt='logo'/></Link>
            </div>
            <ul className='header-top-menu'>
                <li><a className='header-top-item' href='#'>Насіння</a></li>
                <li><a className='header-top-item' href='#'>Саджанці</a></li>
                <li><a className='header-top-item' href='#'>Однорічні</a></li>
                <li><a className='header-top-item' href='#'>Багаторічні</a></li>
                <li><a className='header-top-item' href='#'>Вічнозелені</a></li>
            </ul>
            <div className='header-top-action'>
                <Link to={'/favorites'}><Heart/></Link>
                <span className='favorite-count'>{favorite.length}</span>
                <Link to={'/basket'}><Cart/></Link>
                <span>{shop.length}</span>
            </div>
        </div>
    );
};

Nav.propTypes = {
    favorite: PropTypes.array,
    shop: PropTypes.array
}

export default Nav;