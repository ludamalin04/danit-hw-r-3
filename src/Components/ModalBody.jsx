import PropTypes from "prop-types";

const ModalBody = ({url, title, desc}) => {
    return (
        <div className="modal-body">
            <div><img className='modal-img' src={url} alt={title}/></div>
            <h2 className='modal-title'>{title}</h2>
            <p className='modal-txt'>{desc} грн</p>
        </div>
    );
};

export default ModalBody;

ModalBody.propTypes = {
    img: PropTypes.any,
    title: PropTypes.string,
    url: PropTypes.string,
    desc: PropTypes.number,
}