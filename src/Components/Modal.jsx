import PropTypes from "prop-types";
import Close from "./Close.jsx";
import './Components.scss'

const Modal = ({actionFirst, children}) => {

    const stopClick = (event) => {
        event.stopPropagation()
    }
    return (
        <>
            <div onClick={stopClick} className="modal">
                <Close click={actionFirst}/>
                {children}
            </div>
        </>
    );
};

export default Modal;

Modal.propTypes = {
    children: PropTypes.any,
    actionFirst: PropTypes.func,
}